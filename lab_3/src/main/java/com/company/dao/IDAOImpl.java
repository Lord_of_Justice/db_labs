package com.company.dao;

import java.util.List;

public interface IDAOImpl<T> {
    T getEntity(int id);
    List<T> getEntityList();
    void insertEntity(T entity);
    void updateEntity(int index, T entity) throws IllegalAccessException;
    void deleteEntity(int id);
}

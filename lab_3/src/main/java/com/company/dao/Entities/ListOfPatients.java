package com.company.dao.Entities;

import javax.persistence.*;

@Entity
@Table(name = "public.\"List_Of_Patients\"")
public class ListOfPatients implements MyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"lofPt_id\"")
    private int lofPt_id;
    @Column(name = "\"lofPt_ptid\"")
    private int lofPt_ptid;
    @Column(name = "\"lofPt_docid\"")
    private int lofPt_docid;

    public ListOfPatients(){}
    public ListOfPatients(int ptid, int docid){
        lofPt_ptid = ptid;
        lofPt_docid = docid;
    }
    public void print(){
        System.out.println(String.format("ID = %d; lofPt_ptid = %d, lofPt_docid = %d",
                lofPt_id, lofPt_ptid, lofPt_docid));
    }
}

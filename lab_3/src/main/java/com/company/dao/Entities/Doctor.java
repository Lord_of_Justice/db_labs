package com.company.dao.Entities;

import javax.persistence.*;

@Entity
@Table(name = "public.\"Doctor\"")
public class Doctor implements MyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int doc_id;
    private String doc_name;
    private String doc_specilty;

    public Doctor(){}
    public Doctor(String name, String specilty){
        doc_name = name;
        doc_specilty = specilty;
    }
    public void print() {
        System.out.println(String.format("ID = %d; Name: %s, Specilty: %s",
                doc_id, doc_name, doc_specilty));
    }
}

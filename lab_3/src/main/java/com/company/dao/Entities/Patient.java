package com.company.dao.Entities;

import javax.persistence.*;

@Entity
@Table(name = "public.\"Patient\"")
public class Patient implements MyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int pt_id;
    private String pt_name;
    private int pt_age;
    @Column(name = "\"pt_illnessTime\"")
    private int pt_illnessTime;
    private boolean pt_male;

    public Patient(){}
    public Patient(String name, int age, int illnessTime, boolean male){
        pt_name = name;
        pt_age = age;
        pt_illnessTime = illnessTime;
        pt_male = male;
    }
    public void print(){
        String male = pt_male ? "Male" : "Female";
        System.out.println(String.format("ID = %d; Name: %s (%d), %s, Illness time: %d",
                pt_id, pt_name, pt_age, male, pt_illnessTime));
    }
}

package com.company.dao.Entities;

import javax.persistence.*;

@Entity
@Table(name = "public.\"Medical_Opinion\"")
public class MedicalOpinion implements MyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"medOp_id\"")
    private int medOp_id;
    @Column(name = "\"medOp_docid\"")
    private int medOp_docid;
    @Column(name = "\"medOp_dgid\"")
    private int medOp_dgid;

    public MedicalOpinion(){}
    public MedicalOpinion(int docid, int dgid){
        medOp_docid = docid;
        medOp_dgid = dgid;
    }
    public void print(){
        System.out.println(String.format("ID = %d; medOp_docid = %d, medOp_dgid = %d",
                medOp_id, medOp_docid, medOp_dgid));
    }
}

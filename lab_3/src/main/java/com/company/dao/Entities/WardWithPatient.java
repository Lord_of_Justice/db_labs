package com.company.dao.Entities;

import javax.persistence.*;

@Entity
@Table(name = "public.\"Ward_With_Patient\"")
public class WardWithPatient implements MyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"wrdPt_id\"")
    private int wrdPt_id;
    @Column(name = "\"wrdPt_ptid\"")
    private int wrdPt_ptid;
    @Column(name = "\"wrdPt_wrdid\"")
    private int wrdPt_wrdid;

    public WardWithPatient(){}
    public WardWithPatient(int ptid, int wrdid){
        wrdPt_ptid = ptid;
        wrdPt_wrdid = wrdid;
    }
    public void print(){
        System.out.println(String.format("ID = %d; wrdPt_ptid = %d, wrdPt_wrdid = %d",
                wrdPt_id, wrdPt_ptid, wrdPt_wrdid));
    }
}

package com.company.dao.Entities;

import javax.persistence.*;

@Entity
@Table(name = "public.\"Ward\"")
public class Ward implements MyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int wrd_id;
    private int wrd_number;
    private String wrd_unit;
    @Column(name = "\"wrd_peopleAmount\"")
    private int wrd_peopleAmount;

    public Ward(){}
    public Ward(int number, String unit, int peopleAmount){
        wrd_number = number;
        wrd_unit = unit;
        wrd_peopleAmount = peopleAmount;
    }
    public void print(){
        System.out.println(String.format("ID = %d; number = %d, unit = %s, peopleAmount = %d",
                wrd_id, wrd_number, wrd_unit, wrd_peopleAmount));
    }
}

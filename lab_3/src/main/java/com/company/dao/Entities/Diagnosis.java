package com.company.dao.Entities;

import javax.persistence.*;

@Entity
@Table(name = "public.\"Diagnosis\"")
public class Diagnosis implements MyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int dg_id;
    private String dg_name;
    private String dg_symptoms;
    private String dg_therapy;

    public Diagnosis(){}
    public Diagnosis(String name, String symptoms, String therapy){
        dg_name = name;
        dg_symptoms = symptoms;
        dg_therapy = therapy;
    }

    @Override
    public void print() {
        System.out.println("-------------------------------------------------");
        System.out.println(String.format("ID = %d; Name: %s\nSymptoms: %s\n Therapy %s",
                dg_id, dg_name, dg_symptoms, dg_therapy));
    }
}

package com.company.dao.Entities;

import javax.persistence.*;

@Entity
@Table(name = "public.\"Patient_Diagnose\"")
public class PatientDiagnose implements MyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"ptDia_id\"")
    private int ptDia_id;
    @Column(name = "\"ptDia_ptid\"")
    private int ptDia_ptid;
    @Column(name = "\"ptDia_dgid\"")
    private int ptDia_dgid;

    public PatientDiagnose(){}
    public PatientDiagnose(int ptid, int dgid){
        ptDia_ptid = ptid;
        ptDia_dgid = dgid;
    }
    public void print(){
        System.out.println(String.format("ID = %d; ptDia_ptid = %d, ptDia_dgid = %d",
                ptDia_id, ptDia_ptid, ptDia_dgid));
    }
}

package com.company.dao;

import java.util.Arrays;
import java.util.List;
import com.company.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;

public class DAOImpl<T> implements IDAOImpl<T> {
    Class<T> clazz;

    public DAOImpl(Class<T> clazz){
        this.clazz = clazz;
    }
    public T getEntity(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(clazz, id);
    }
    public void insertEntity(T entity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(entity);
        tx1.commit();
        session.close();
    }
    public void updateEntity(int index, T entity){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        T loadEntity = getEntity(index);
        List<Field> fields = Arrays.asList(entity.getClass().getDeclaredFields());
        try {
            for(int i = 1; i < fields.size(); i++){
                Field f = fields.get(i);
                f.setAccessible(true);
                f.set(loadEntity, f.get(entity));
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        session.update(loadEntity);
        tx1.commit();
        session.close();
    }
    public void deleteEntity(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        T entity = session.load(clazz, id);
        session.delete(entity);
        tx1.commit();
        session.close();
    }
    public List<T> getEntityList() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(clazz);
        Root<T> rootEntry = cq.from(clazz);
        CriteriaQuery<T> all = cq.select(rootEntry);

        TypedQuery<T> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }
}
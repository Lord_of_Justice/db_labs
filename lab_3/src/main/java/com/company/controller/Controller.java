package com.company.controller;

import com.company.controller.CRUDOperations.*;
import com.company.dao.Entities.Patient;
import java.util.Random;

public class Controller {
    public DiagnoseCrud diagnose;
    public DoctorCrud doctor;
    public ListOfPatientsCrud listOfPatients;
    public MedicalOpinionCrud medicalOpinion;
    public PatientCrud patient;
    public PatientDiagnoseCrud patientDiagnose;
    public WardCrud ward;
    public WardWithPatientCrud wardWithPatient;

    public Controller() {
        diagnose = new DiagnoseCrud();
        doctor = new DoctorCrud();
        listOfPatients = new ListOfPatientsCrud();
        medicalOpinion = new MedicalOpinionCrud();
        patient = new PatientCrud();
        patientDiagnose = new PatientDiagnoseCrud();
        ward = new WardCrud();
        wardWithPatient = new WardWithPatientCrud();
    }

    public void generateRandomDataInPatient(int amount){
        Random rnd = new Random();
        for (int i = 0; i < amount; i++) {
            boolean randomMale = rnd.nextBoolean();
            String randomName = "";
            int number = 0;
            if (randomMale) {
                number = rnd.nextInt(10);
            } else {
                number = 10 + rnd.nextInt(20 - 10);
            }
            randomName = Names.values()[number].toString();
            int randomAge = rnd.nextInt(91);
            int randomIllnessTime = rnd.nextInt(151);
            Patient insertPatient = new Patient(randomName, randomAge, randomIllnessTime, randomMale);
            patient.insert(insertPatient);
        }
    }
}

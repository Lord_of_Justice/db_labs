package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.dao.Entities.PatientDiagnose;

import java.util.List;

public class PatientDiagnoseCrud {
    DAOImpl<PatientDiagnose> dao;

    public PatientDiagnoseCrud(){
        dao = new DAOImpl<>(PatientDiagnose.class);
    }

    public PatientDiagnose get(int id){
        return dao.getEntity(id);
    }
    public List<PatientDiagnose> getAll(){
        return dao.getEntityList();
    }

    public void insert(PatientDiagnose patientDiagnose){
        dao.insertEntity(patientDiagnose);
    }
    public void update(int id, PatientDiagnose patientDiagnose){
        dao.updateEntity(id, patientDiagnose);
    }
    public void delete(int id){
        dao.deleteEntity(id);
    }
}

package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.dao.Entities.Ward;

import java.util.List;

public class WardCrud {
    DAOImpl<Ward> dao;

    public WardCrud(){
        dao = new DAOImpl<>(Ward.class);
    }

    public Ward get(int id){
        return dao.getEntity(id);
    }
    public List<Ward> getAll(){
        return dao.getEntityList();
    }
    public void insert(Ward ward){
        dao.insertEntity(ward);
    }
    public void update(int id, Ward ward){
        dao.updateEntity(id, ward);
    }
    public void delete(int id){
        dao.deleteEntity(id);
    }
}

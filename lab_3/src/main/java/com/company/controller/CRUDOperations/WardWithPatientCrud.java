package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.dao.Entities.WardWithPatient;
import java.util.List;

public class WardWithPatientCrud {
    DAOImpl<WardWithPatient> dao;

    public WardWithPatientCrud(){
        dao = new DAOImpl<>(WardWithPatient.class);
    }

    public WardWithPatient get(int id){
        return dao.getEntity(id);
    }
    public List<WardWithPatient> getAll(){
        return dao.getEntityList();
    }
    public void insert(WardWithPatient wardWithPatientatient){
        dao.insertEntity(wardWithPatientatient);
    }
    public void update(int id, WardWithPatient wardWithPatientatient){
        dao.updateEntity(id, wardWithPatientatient);
    }
    public void delete(int id){
        dao.deleteEntity(id);
    }
}

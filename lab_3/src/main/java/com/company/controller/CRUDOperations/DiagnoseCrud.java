package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.dao.Entities.Diagnosis;
import java.util.List;

public class DiagnoseCrud {
    DAOImpl<Diagnosis> dao;

    public DiagnoseCrud(){
        dao = new DAOImpl<>(Diagnosis.class);
    }

    public Diagnosis get(int id){
        return dao.getEntity(id);
    }
    public List<Diagnosis> getAll(){
        return dao.getEntityList();
    }
    public void insert(Diagnosis diagnose){
        dao.insertEntity(diagnose);
    }
    public void update(int id, Diagnosis diagnose){
        dao.updateEntity(id, diagnose);
    }
    public void delete(int id){
        dao.deleteEntity(id);
    }
    /*public ResultSet fullTextSearch(String word, boolean isPhrase){
        return isPhrase ? dao.phraseSearch(word) : dao.wordSearch(word);
    }*/
}

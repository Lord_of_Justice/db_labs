package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.dao.Entities.Doctor;
import java.util.List;

public class DoctorCrud {
    DAOImpl<Doctor> dao;

    public DoctorCrud(){
        dao = new DAOImpl<>(Doctor.class);
    }

    public Doctor get(int id){
        return dao.getEntity(id);
    }
    public List<Doctor> getAll(){
        return dao.getEntityList();
    }
    public void insert(Doctor doctor){
        dao.insertEntity(doctor);
    }
    public void update(int id, Doctor patient){
        dao.updateEntity(id, patient);
    }
    public void delete(int id){
        dao.deleteEntity(id);
    }
}

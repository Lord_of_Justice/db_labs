package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.dao.Entities.MedicalOpinion;

import java.util.List;

public class MedicalOpinionCrud {
    DAOImpl<MedicalOpinion> dao;

    public MedicalOpinionCrud(){
        dao = new DAOImpl<>(MedicalOpinion.class);
    }

    public MedicalOpinion get(int id){
        return dao.getEntity(id);
    }
    public List<MedicalOpinion> getAll(){
        return dao.getEntityList();
    }
    public void insert(MedicalOpinion opinion){
        dao.insertEntity(opinion);
    }
    public void update(int id, MedicalOpinion opinion){
        dao.updateEntity(id, opinion);
    }
    public void delete(int id){
        dao.deleteEntity(id);
    }
}

package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.dao.Entities.ListOfPatients;
import java.util.List;

public class ListOfPatientsCrud {
    DAOImpl<ListOfPatients> dao;

    public ListOfPatientsCrud( ){
        dao = new DAOImpl<>(ListOfPatients.class);
    }

    public ListOfPatients get(int id){
        return dao.getEntity(id);
    }
    public List<ListOfPatients> getAll(){
        return dao.getEntityList();
    }
    public void insert(ListOfPatients listOfPatients){
        dao.insertEntity(listOfPatients);
    }
    public void update(int id, ListOfPatients listOfPatients){
        dao.updateEntity(id, listOfPatients);
    }
    public void delete(int id){
        dao.deleteEntity(id);
    }
}

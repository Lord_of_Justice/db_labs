package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.dao.Entities.Patient;

import java.util.List;

public class PatientCrud {
    DAOImpl<Patient> dao;

    public PatientCrud(){
        dao = new DAOImpl<>(Patient.class);
    }

    public Patient get(int id) {
        return dao.getEntity(id);
    }
    public List<Patient> getAll(){
        return dao.getEntityList();
    }
    public void insert(Patient patient){
        dao.insertEntity(patient);
    }
    public void update(int id, Patient patient){
        dao.updateEntity(id, patient);
    }
    public void delete(int id){
        dao.deleteEntity(id);
    }

}

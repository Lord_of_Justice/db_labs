package com.company.utils;

import com.company.dao.Entities.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;

    private HibernateSessionFactoryUtil() {}

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                //disableLogging();
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(Diagnosis.class);
                configuration.addAnnotatedClass(Doctor.class);
                configuration.addAnnotatedClass(ListOfPatients.class);
                configuration.addAnnotatedClass(MedicalOpinion.class);
                configuration.addAnnotatedClass(Patient.class);
                configuration.addAnnotatedClass(PatientDiagnose.class);
                configuration.addAnnotatedClass(Ward.class);
                configuration.addAnnotatedClass(WardWithPatient.class);
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());

            } catch (Exception e) {
                System.out.println("Исключение!" + e);
            }
        }
        return sessionFactory;
    }
    // for disable logger from hibernate
    private static void disableLogging() {
        LogManager logManager = LogManager.getLogManager();
        Logger logger = logManager.getLogger("");
        logger.setLevel(Level.SEVERE); //could be Level.OFF
    }
}
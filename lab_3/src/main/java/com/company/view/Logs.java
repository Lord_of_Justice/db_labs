package com.company.view;

public class Logs {
    public void showMainMenu() {
        System.out.println("========== Main Menu ==========");
        System.out.println("Choose table or operation: ");
        for (int i = 0; i < TableNames.values().length; i++){
            System.out.println(
                    String.format("%d) Table %s", i + 1, TableNames.values()[i].toString()));
        }
        System.out.println("9) Random gen");
        System.out.println("10) Exit\n-> ");
    }
    public void showMenuTableOperations(String tableName){
        System.out.println("========== Table " + tableName + " Menu ==========" );
        System.out.println("1) ShowAll\n2) Find by index");
        System.out.println("3) Insert\n4) Update");
        System.out.println("5) Delete\n6) To Main Menu\n-> ");
    }
}

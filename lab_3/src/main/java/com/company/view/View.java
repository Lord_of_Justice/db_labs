package com.company.view;

import com.company.controller.Controller;
import com.company.dao.Entities.*;

import java.util.List;
import java.util.Scanner;

public class View {
    Controller controller;
    Logs logs;
    Scanner scanner;

    public View(){
        controller = new Controller();
        logs = new Logs();
    }
    public void mainMenu(){
        scanner = new Scanner(System.in);
        while(true){
            logs.showMainMenu();
            int number = Integer.parseInt(scanner.nextLine());
            if (number > 0 && number < 9){
                tablesMenu(number);
            } else if (number == 9){
                System.out.println("Write amount: ");
                int amount = Integer.parseInt(scanner.nextLine());
                controller.generateRandomDataInPatient(amount);
            } else if (number == 10){
                break;
            }
        }
        scanner.close();
    }
    private void tablesMenu(int number){
        int choice = 0;
        do{
            logs.showMenuTableOperations(TableNames.values()[number - 1].toString());
            try {
                choice = Integer.parseInt(scanner.nextLine());
                switch (choice) {
                    case 1:
                        showAllEntities(number);
                        break;
                    case 2:
                        FindEntityById(number);
                        break;
                    case 3:
                        insertEntityById(number);
                        break;
                    case 4:
                        updateEntityById(number);
                        break;
                    case 5:
                        deleteEntityById(number);
                        break;
                }
            } catch (Exception e){
                e.getMessage();
            }
        } while (choice != 6);
    }

    private <T> void showAllEntities(int number) {
        if (number == 1) {
            List<Diagnosis> diagnoses = controller.diagnose.getAll();
            printEntity(diagnoses);
        } else if (number == 2) {
            List<Doctor> doctors = controller.doctor.getAll();
            printEntity(doctors);
        } else if(number == 3) {
            List<ListOfPatients> list = controller.listOfPatients.getAll();
            printEntity(list);
        } else if(number == 4) {
            List<MedicalOpinion> list = controller.medicalOpinion.getAll();
            printEntity(list);
        } else if (number == 5) {
            List<Patient> list = controller.patient.getAll();
            printEntity(list);
        } else if (number == 6) {
            List<PatientDiagnose> list = controller.patientDiagnose.getAll();
            printEntity(list);
        } else if (number == 7) {
            List<Ward> list = controller.ward.getAll();
            printEntity(list);
        } else if (number == 8) {
            List<WardWithPatient> list = controller.wardWithPatient.getAll();
            printEntity(list);
        }
    }
    private void FindEntityById(int number){
        MyEntity myEntity = null;
        System.out.println("Write index: ");
        int index = Integer.parseInt(scanner.nextLine());
        if (number == 1) {
            myEntity = controller.diagnose.get(index);
        } else if (number == 2) {
            myEntity = controller.doctor.get(index);
        } else if(number == 3) {
            myEntity = controller.listOfPatients.get(index);
        } else if(number == 4) {
            myEntity = controller.medicalOpinion.get(index);
        } else if (number == 5) {
            myEntity = controller.patient.get(index);
        } else if (number == 6) {
            myEntity = controller.patientDiagnose.get(index);
        } else if (number == 7) {
            myEntity = controller.ward.get(index);
        } else if (number == 8) {
            myEntity = controller.wardWithPatient.get(index);
        }
        myEntity.print();
    }
    private void insertEntityById(int number) {
        if (number == 1) {
            System.out.println("Write name: ");
            String name = scanner.nextLine();
            System.out.println("Write symptoms: ");
            String symptoms = scanner.nextLine();
            System.out.println("Write therapy: ");
            String therapy = scanner.nextLine();
            controller.diagnose.insert(new Diagnosis(name, symptoms, therapy));
        } else if (number == 2) {
            System.out.println("Write name: ");
            String name = scanner.nextLine();
            System.out.println("Write specilty: ");
            String specilty = scanner.nextLine();
            controller.doctor.insert(new Doctor(name, specilty));
        } else if(number == 3) {
            System.out.println("Write patient index: ");
            int ptid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write doctor index: ");
            int docid = Integer.parseInt(scanner.nextLine());
            controller.listOfPatients.insert(new ListOfPatients(ptid, docid));
        } else if(number == 4) {
            System.out.println("Write doctor index: ");
            int docid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write diagnose index: ");
            int dgid = Integer.parseInt(scanner.nextLine());
            controller.medicalOpinion.insert(new MedicalOpinion(docid, dgid));
        } else if (number == 5) {
            System.out.println("Write name: ");
            String name = scanner.nextLine();
            System.out.println("Write age: ");
            int age = Integer.parseInt(scanner.nextLine());
            System.out.println("Write illness time: ");
            int illnessTime = Integer.parseInt(scanner.nextLine());
            System.out.println("Write male: ");
            boolean male = Boolean.parseBoolean(scanner.nextLine());
            controller.patient.insert(new Patient(name, age, illnessTime, male));
        } else if (number == 6) {
            System.out.println("Write patient index: ");
            int ptid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write diagnose index: ");
            int dgid = Integer.parseInt(scanner.nextLine());
            controller.patientDiagnose.insert(new PatientDiagnose(ptid, dgid));
        } else if (number == 7) {
            System.out.println("Write patient index: ");
            int wdnumber = Integer.parseInt(scanner.nextLine());
            System.out.println("Write name: ");
            String unit = scanner.nextLine();
            System.out.println("Write patient index: ");
            int peopleAmount = Integer.parseInt(scanner.nextLine());
            controller.ward.insert(new Ward(wdnumber, unit, peopleAmount));
        } else if (number == 8) {
            System.out.println("Write patient index: ");
            int ptid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write ward index: ");
            int wrdid = Integer.parseInt(scanner.nextLine());
            controller.wardWithPatient.insert(new WardWithPatient(ptid, wrdid));
        }
    }
    private void updateEntityById(int number) {
        System.out.println("Write index: ");
        int index = Integer.parseInt(scanner.nextLine());
        if (number == 1) {
            System.out.println("Write name: ");
            String name = scanner.nextLine();
            System.out.println("Write symptoms: ");
            String symptoms = scanner.nextLine();
            System.out.println("Write therapy: ");
            String therapy = scanner.nextLine();
            controller.diagnose.update(index, new Diagnosis(name, symptoms, therapy));
        } else if (number == 2) {
            System.out.println("Write name: ");
            String name = scanner.nextLine();
            System.out.println("Write specilty: ");
            String specilty = scanner.nextLine();
            controller.doctor.update(index, new Doctor(name, specilty));
        } else if(number == 3) {
            System.out.println("Write patient index: ");
            int ptid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write doctor index: ");
            int docid = Integer.parseInt(scanner.nextLine());
            controller.listOfPatients.update(index, new ListOfPatients(ptid, docid));
        } else if(number == 4) {
            System.out.println("Write doctor index: ");
            int docid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write diagnose index: ");
            int dgid = Integer.parseInt(scanner.nextLine());
            controller.medicalOpinion.update(index, new MedicalOpinion(docid, dgid));
        } else if (number == 5) {
            System.out.println("Write name: ");
            String name = scanner.nextLine();
            System.out.println("Write age: ");
            int age = Integer.parseInt(scanner.nextLine());
            System.out.println("Write illness time: ");
            int illnessTime = Integer.parseInt(scanner.nextLine());
            System.out.println("Write male: ");
            boolean male = Boolean.parseBoolean(scanner.nextLine());
            controller.patient.update(index, new Patient(name, age, illnessTime, male));
        } else if (number == 6) {
            System.out.println("Write patient index: ");
            int ptid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write diagnose index: ");
            int dgid = Integer.parseInt(scanner.nextLine());
            controller.patientDiagnose.update(index, new PatientDiagnose(ptid, dgid));
        } else if (number == 7) {
            System.out.println("Write patient index: ");
            int wdnumber = Integer.parseInt(scanner.nextLine());
            System.out.println("Write name: ");
            String unit = scanner.nextLine();
            System.out.println("Write patient index: ");
            int peopleAmount = Integer.parseInt(scanner.nextLine());
            controller.ward.update(index, new Ward(wdnumber, unit, peopleAmount));
        } else if (number == 8) {
            System.out.println("Write patient index: ");
            int ptid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write ward index: ");
            int wrdid = Integer.parseInt(scanner.nextLine());
            controller.wardWithPatient.update(index, new WardWithPatient(ptid, wrdid));
        }
    }
    private void deleteEntityById(int number){
        System.out.println("Write index: ");
        int index = Integer.parseInt(scanner.nextLine());
        if (number == 1) {
            controller.diagnose.delete(index);
        } else if (number == 2) {
            controller.doctor.delete(index);
        } else if(number == 3) {
            controller.listOfPatients.delete(index);
        } else if(number == 4) {
            controller.medicalOpinion.delete(index);
        } else if (number == 5) {
            controller.patient.delete(index);
        } else if (number == 6) {
            controller.patientDiagnose.delete(index);
        } else if (number == 7) {
            controller.ward.delete(index);
        } else if (number == 8) {
            controller.wardWithPatient.delete(index);
        }
    }

    private <T> void printEntity(List<T> list){
        for (T t: list) {
            MyEntity myEntity = (MyEntity) t;
            myEntity.print();
        }
    }
}

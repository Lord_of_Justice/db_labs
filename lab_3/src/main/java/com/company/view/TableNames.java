package com.company.view;

public enum TableNames {
    Diagnosis,
    Doctor,
    List_Of_Patients,
    Medical_Opinion,
    Patient,
    Patient_Diagnose,
    Ward,
    Ward_With_Patient
}

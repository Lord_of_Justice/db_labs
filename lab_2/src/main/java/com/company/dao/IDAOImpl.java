package com.company.dao;

import java.sql.SQLException;
import java.util.List;

public interface IDAOImpl<T> {
    T getEntity(int id) throws SQLException;
    List<T> getEntityList()  throws SQLException;
    void insertEntity(T entity) throws SQLException, IllegalAccessException;
    void updateEntity(T entity) throws SQLException, IllegalAccessException;
    void deleteEntity(int id) throws SQLException;
}

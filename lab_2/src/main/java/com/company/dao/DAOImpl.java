package com.company.dao;

import com.company.controller.Entities.TableName;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DAOImpl<T> implements IDAOImpl<T> {
    Class<T> clazz;
    private Connection connection;

    public DAOImpl(Class<T> clazz){
        this.clazz = clazz;
        try {
            this.connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Hospital",
                    "admin", "admin");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String getIdString(){
        return clazz.getAnnotation(TableName.class).shortName() + "_id";
    }
    private String getTableNameString(){
        return "public.\"" + clazz.getAnnotation(TableName.class).name() + "\"";
    }
    private T createEntity(ResultSet rs){
        T entity;
        try {
            entity = clazz.getConstructor().newInstance();
            List<Field> fields = Arrays.asList(clazz.getDeclaredFields());
            for(Field f: fields){
                f.setAccessible(true);
                try{
                    Object value = rs.getObject(f.getName());
                    f.set(entity, value);
                } catch (SQLException e){
                    e.printStackTrace();
                }
            }
        } catch (IllegalAccessException
                | InvocationTargetException
                | NoSuchMethodException
                | InstantiationException e){
            e.printStackTrace();
            entity = null;
        }
        return entity;
    }

    public T getEntity(int id) throws SQLException{
        String query = String.format("SELECT * FROM %s WHERE %s = %d", getTableNameString(), getIdString(), id);
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(query);
        T entity = rs.next() ? createEntity(rs) : null;
        rs.close();
        st.close();
        return entity;
    }
    public List<T> getEntityList() throws SQLException{
        Statement st = connection.createStatement();
        ResultSet resultSet = st.executeQuery("select * from " + getTableNameString());

        List<T> entities = new ArrayList<>();
        while(resultSet.next()) {
            entities.add(createEntity(resultSet));
        }
        resultSet.close();
        st.close();
        return entities;
    }
    public void insertEntity(T entity) throws SQLException, IllegalAccessException {
        List<Field> fields = Arrays.asList(entity.getClass().getDeclaredFields());

        String table = getTableNameString() + "(";
        String values = "";
        for (Field f: fields){
            table = table.concat("\"" + f.getName() + "\",");
            values = values.concat(" ?,");
        }
        table = table.substring(0, table.length() - 1) + ')';
        values = values.substring(0, values.length() - 1);
        System.out.println(values);

        PreparedStatement prest = connection.prepareStatement(
                String.format("INSERT INTO %s VALUES (%s)", table, values));
        int i = 1;
        for (Field f: fields){
            f.setAccessible(true);
            prest.setObject(i, f.get(entity));
            i++;
        }
        prest.executeUpdate();
        prest.close();
    }
    public void updateEntity(T entity) throws SQLException, IllegalAccessException {
        List<Field> fields = new ArrayList<>();
        fields.addAll(Arrays.asList(entity.getClass().getDeclaredFields()));

        String valuesForUpdate = "";
        for (Field f: fields){
            f.setAccessible(true);
            valuesForUpdate = valuesForUpdate.concat("\""+f.getName() + "\" = ?,");
        }
        valuesForUpdate = valuesForUpdate.substring(0, valuesForUpdate.length() - 1);
        System.out.println(valuesForUpdate);

        int id = (int)fields.get(0).get(entity);

        PreparedStatement prest = connection.prepareStatement(
                String.format("UPDATE %s SET %s WHERE %s = " + id,
                        getTableNameString(), valuesForUpdate, getIdString()));
        int i = 1;
        for (Field f: fields){
            prest.setObject(i, f.get(entity));
            i++;
        }
        prest.executeUpdate();
        prest.close();
    }
    public void deleteEntity(int id) throws SQLException {
        String query = String.format("DELETE FROM %s WHERE %s = %d",
                getTableNameString(), getIdString(), id);
        Statement st = connection.createStatement();
        st.execute(query);
        st.close();
    }

    public int getEntityMaxId() throws SQLException {
        Statement st = connection.createStatement();
        String query = String.format("select MAX(%s) \"maxId\" from %s", getIdString(), getTableNameString());
        ResultSet resultSet = st.executeQuery(query);

        int maxId = 0;
        if(resultSet.next()) {
            maxId = resultSet.getInt("maxId");
        }
        resultSet.close();
        st.close();
        return maxId;
    }

    public ResultSet searchThroughThreeTables(int leftBound, int rightBound, boolean male){
        String query = "SELECT \"pt_id\",\"pt_name\",\"pt_age\",\"pt_illnessTime\",\"pt_male\",\n" +
                "\"dg_name\",\"dg_symptoms\",\"dg_therapy\"" +
                "\tFROM \"Patient\" pt \n" +
                "\tINNER JOIN \"Patient_Diagnose\" ptDis \n" +
                "\ton pt.\"pt_id\" = ptDis.\"ptDia_ptid\"\n" +
                "\tINNER JOIN \"Diagnosis\" dis\n" +
                "\ton dis.\"dg_id\" = ptDis.\"ptDia_dgid\"\n" +
                "\tWHERE (pt.\"pt_age\" BETWEEN ? AND ?) and pt.\"pt_male\" = ?";
        ResultSet rs = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, leftBound);
            preparedStatement.setInt(2, rightBound);
            preparedStatement.setBoolean(3, male);
            rs = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }
    public ResultSet wordSearch(String word) {
        ResultSet rs = null;
        try{
            String sqlWordSearch = "SELECT \"dg_name\" , ts_headline(\"dg_symptoms\",q,'StartSel=<!>, StopSel=<!>') dg_symptoms\n" +
                    "     FROM \"Diagnosis\", to_tsquery(?) as q\n" +
                    "     WHERE to_tsvector(\"dg_symptoms\") @@ q";
            PreparedStatement prpSearchStmt = connection.prepareStatement(sqlWordSearch);
            prpSearchStmt.setString(1 , word);
            rs = prpSearchStmt.executeQuery();
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return rs;
    }
    public ResultSet phraseSearch(String phrase) {
        ResultSet rs = null;
        try{
            String sqlWordSearch = "SELECT \"dg_name\" , ts_headline(\"dg_symptoms\",q,'StartSel=<!>, StopSel=<!>') dg_symptoms\n" +
                    "     FROM \"Diagnosis\", phraseto_tsquery(?) as q\n" +
                    "     WHERE to_tsvector(\"dg_symptoms\") @@ q";
            PreparedStatement prpSearchStmt = connection.prepareStatement(sqlWordSearch);
            prpSearchStmt.setString(1 , phrase);
            rs = prpSearchStmt.executeQuery();
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return rs;
    }

}
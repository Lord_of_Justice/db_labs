package com.company.controller.Entities;

@TableName(name = "Patient", shortName = "pt")
public class Patient implements Entity {
    private int pt_id;
    private String pt_name;
    private int pt_age;
    private int pt_illnessTime;
    private boolean pt_male;

    public Patient(){}
    public Patient(int id, String name, int age, int illnessTime, boolean male){
        pt_id = id;
        pt_name = name;
        pt_age = age;
        pt_illnessTime = illnessTime;
        pt_male = male;
    }
    public void print(){
        String male = pt_male ? "Male" : "Female";
        System.out.println(String.format("ID = %d; Name: %s (%d), %s, Illness time: %d",
                pt_id, pt_name, pt_age, male, pt_illnessTime));
    }
}

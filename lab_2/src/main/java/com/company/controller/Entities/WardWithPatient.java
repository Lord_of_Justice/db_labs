package com.company.controller.Entities;

@TableName(name = "Ward_With_Patient", shortName = "wrdPt")
public class WardWithPatient implements Entity {
    private int wrdPt_id;
    private int wrdPt_ptid;
    private int wrdPt_wrdid;

    public WardWithPatient(){}
    public WardWithPatient(int id, int ptid, int wrdid){
        wrdPt_id = id;
        wrdPt_ptid = ptid;
        wrdPt_wrdid = wrdid;
    }
    public void print(){
        System.out.println(String.format("ID = %d; wrdPt_ptid = %d, wrdPt_wrdid = %d",
                wrdPt_id, wrdPt_ptid, wrdPt_wrdid));
    }
}

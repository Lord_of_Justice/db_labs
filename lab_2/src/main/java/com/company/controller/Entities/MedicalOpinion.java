package com.company.controller.Entities;

@TableName(name = "Medical_Opinion", shortName = "medOp")
public class MedicalOpinion implements Entity {
    private int medOp_id;
    private int medOp_docid;
    private int medOp_dgid;

    public MedicalOpinion(){}
    public MedicalOpinion(int id, int docid, int dgid){
        medOp_id = id;
        medOp_docid = docid;
        medOp_dgid = dgid;
    }
    public void print(){
        System.out.println(String.format("ID = %d; medOp_docid = %d, medOp_dgid = %d",
                medOp_id, medOp_docid, medOp_dgid));
    }
}

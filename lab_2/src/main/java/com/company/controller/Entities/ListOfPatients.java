package com.company.controller.Entities;

@TableName(name = "List_Of_Patients", shortName = "lofPt")
public class ListOfPatients implements Entity {
    private int lofPt_id;
    private int lofPt_ptid;
    private int lofPt_docid;

    public ListOfPatients(){}
    public ListOfPatients(int id, int ptid, int docid){
        lofPt_id = id;
        lofPt_ptid = ptid;
        lofPt_docid = docid;
    }
    public void print(){
        System.out.println(String.format("ID = %d; lofPt_ptid = %d, lofPt_docid = %d",
                lofPt_id, lofPt_ptid, lofPt_docid));
    }
}

package com.company.controller.Entities;

@TableName(name = "Ward", shortName = "wrd")
public class Ward implements Entity {
    private int wrd_id;
    private int wrd_number;
    private String wrd_unit;
    private int wrd_peopleAmount;

    public Ward(){}
    public Ward(int id, int number, String unit, int peopleAmount){
        wrd_id = id;
        wrd_number = number;
        wrd_unit = unit;
        wrd_peopleAmount = peopleAmount;
    }
    public void print(){
        System.out.println(String.format("ID = %d; number = %d, unit = %s, peopleAmount = %d",
                wrd_id, wrd_number, wrd_unit, wrd_peopleAmount));
    }
}

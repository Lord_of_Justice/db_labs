package com.company.controller.Entities;

@TableName(name = "Diagnosis", shortName = "dg")
public class Diagnosis implements Entity {
    private int dg_id;
    private String dg_name;
    private String dg_symptoms;
    private String dg_therapy;

    public Diagnosis(){}
    public Diagnosis(int id, String name, String symptoms, String therapy){
        dg_id = id;
        dg_name = name;
        dg_symptoms = symptoms;
        dg_therapy = therapy;
    }

    @Override
    public void print() {
        System.out.println("-------------------------------------------------");
        System.out.println(String.format("ID = %d; Name: %s\nSymptoms: %s\n Therapy %s",
                dg_id, dg_name, dg_symptoms, dg_therapy));
    }
}

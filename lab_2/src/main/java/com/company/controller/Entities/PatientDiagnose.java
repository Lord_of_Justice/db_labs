package com.company.controller.Entities;

@TableName(name = "Patient_Diagnose", shortName = "ptDia")
public class PatientDiagnose implements Entity {
    private int ptDia_id;
    private int ptDia_ptid;
    private int ptDia_dgid;

    public PatientDiagnose(){}
    public PatientDiagnose(int id, int ptid, int dgid){
        ptDia_id = id;
        ptDia_ptid = ptid;
        ptDia_dgid = dgid;
    }
    public void print(){
        System.out.println(String.format("ID = %d; ptDia_ptid = %d, ptDia_dgid = %d",
                ptDia_id, ptDia_ptid, ptDia_dgid));
    }
}

package com.company.controller.Entities;

@TableName(name = "Doctor", shortName = "doc")
public class Doctor implements Entity {
    private int doc_id;
    private String doc_name;
    private String doc_specilty;

    public Doctor(){}
    public Doctor(int id, String name, String specilty){
        doc_id = id;
        doc_name = name;
        doc_specilty = specilty;
    }
    @Override
    public void print() {
        System.out.println(String.format("ID = %d; Name: %s, Specilty: %s",
                doc_id, doc_name, doc_specilty));
    }
}

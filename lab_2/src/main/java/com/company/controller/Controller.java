package com.company.controller;

import com.company.controller.CRUDOperations.*;
import com.company.controller.Entities.Patient;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

public class Controller {
    public DiagnoseCrud diagnose;
    public DoctorCrud doctor;
    public ListOfPatientsCrud listOfPatients;
    public MedicalOpinionCrud medicalOpinion;
    public PatientCrud patient;
    public PatientDiagnoseCrud patientDiagnose;
    public WardCrud ward;
    public WardWithPatientCrud wardWithPatient;

    public Controller() {
        diagnose = new DiagnoseCrud();
        doctor = new DoctorCrud();
        listOfPatients = new ListOfPatientsCrud();
        medicalOpinion = new MedicalOpinionCrud();
        patient = new PatientCrud();
        patientDiagnose = new PatientDiagnoseCrud();
        ward = new WardCrud();
        wardWithPatient = new WardWithPatientCrud();
    }

    public void generateRandomDataInPatient(int amount){
        int idList = patient.getMaxId();
        Random rnd = new Random();
        for (int i = idList + 1; i < amount + idList + 1; i++) {
            boolean randomMale = rnd.nextBoolean();
            String randomName = "";
            int number = 0;
            if (randomMale) {
                number = rnd.nextInt(10);
            } else {
                number = 10 + rnd.nextInt(20 - 10);
            }
            randomName = Names.values()[number].toString();
            int randomAge = rnd.nextInt(91);
            int randomIllnessTime = rnd.nextInt(151);
            Patient insertPatient = new Patient(i, randomName, randomAge, randomIllnessTime, randomMale);
            patient.insert(insertPatient);
        }
    }
    public ResultSet StaticSearch1(){
        int leftBound = 0;
        int rightBound = 50;
        return patient.search(leftBound, rightBound, true);
    }
    public ResultSet StaticSearch2(){
        int leftBound = 40;
        int rightBound = 70;
        return patient.search(leftBound, rightBound, false);
    }
    public ResultSet DynamicSearch(int leftBound, int rightBound, boolean male){
        return patient.search(leftBound, rightBound, male);
    }
    public void FullTextSearch(String word, boolean isPhrase){
        diagnose.fullTextSearch(word, isPhrase);
    }
}

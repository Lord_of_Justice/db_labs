package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.controller.Entities.Patient;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class PatientCrud {
    DAOImpl<Patient> dao;

    public PatientCrud(){
        dao = new DAOImpl<>(Patient.class);
    }

    public Patient get(int id) {
        try{
            return dao.getEntity(id);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    public List<Patient> getAll(){
        try{
            return dao.getEntityList();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    public void insert(Patient patient){
        try{
            dao.insertEntity(patient);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void update(Patient patient){
        try{
            dao.updateEntity(patient);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void delete(int id){
        try{
            dao.deleteEntity(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public int getMaxId(){
        try {
            return dao.getEntityMaxId();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
    public ResultSet search(int leftBound, int rightBound, boolean male) {
        return dao.searchThroughThreeTables(leftBound, rightBound, male);
    }

}

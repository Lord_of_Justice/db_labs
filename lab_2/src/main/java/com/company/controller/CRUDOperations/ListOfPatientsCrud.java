package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.controller.Entities.ListOfPatients;

import java.sql.SQLException;
import java.util.List;

public class ListOfPatientsCrud {
    DAOImpl<ListOfPatients> dao;

    public ListOfPatientsCrud( ){
        dao = new DAOImpl<>(ListOfPatients.class);
    }

    public ListOfPatients get(int id) throws SQLException {
        return dao.getEntity(id);
    }
    public List<ListOfPatients> getAll() throws SQLException{
        return dao.getEntityList();
    }
    public void insert(ListOfPatients listOfPatients){
        try{
            dao.insertEntity(listOfPatients);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void update(ListOfPatients listOfPatients){
        try{
            dao.updateEntity(listOfPatients);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void delete(int id){
        try{
            dao.deleteEntity(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

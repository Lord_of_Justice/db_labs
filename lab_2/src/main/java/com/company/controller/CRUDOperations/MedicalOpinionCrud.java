package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.controller.Entities.MedicalOpinion;

import java.sql.SQLException;
import java.util.List;

public class MedicalOpinionCrud {
    DAOImpl<MedicalOpinion> dao;

    public MedicalOpinionCrud(){
        dao = new DAOImpl<>(MedicalOpinion.class);
    }

    public MedicalOpinion get(int id) throws SQLException {
        return dao.getEntity(id);
    }
    public List<MedicalOpinion> getAll() throws SQLException{
        return dao.getEntityList();
    }
    public void insert(MedicalOpinion opinion){
        try{
            dao.insertEntity(opinion);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void update(MedicalOpinion opinion){
        try{
            dao.updateEntity(opinion);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void delete(int id){
        try{
            dao.deleteEntity(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.controller.Entities.Diagnosis;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class DiagnoseCrud {
    DAOImpl<Diagnosis> dao;

    public DiagnoseCrud(){
        dao = new DAOImpl<>(Diagnosis.class);
    }

    public Diagnosis get(int id) throws SQLException {
        return dao.getEntity(id);
    }
    public List<Diagnosis> getAll() throws SQLException{
        return dao.getEntityList();
    }
    public void insert(Diagnosis diagnose){
        try{
            dao.insertEntity(diagnose);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void update(Diagnosis diagnose){
        try{
            dao.updateEntity(diagnose);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void delete(int id){
        try{
            dao.deleteEntity(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public ResultSet fullTextSearch(String word, boolean isPhrase){
        return isPhrase ? dao.phraseSearch(word) : dao.wordSearch(word);
    }
}

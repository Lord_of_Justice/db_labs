package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.controller.Entities.WardWithPatient;

import java.sql.SQLException;
import java.util.List;

public class WardWithPatientCrud {
    DAOImpl<WardWithPatient> dao;

    public WardWithPatientCrud(){
        dao = new DAOImpl<>(WardWithPatient.class);
    }

    public WardWithPatient get(int id) throws SQLException {
        return dao.getEntity(id);
    }
    public List<WardWithPatient> getAll() throws SQLException{
        return dao.getEntityList();
    }
    public void insert(WardWithPatient wardWithPatientatient){
        try{
            dao.insertEntity(wardWithPatientatient);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void update(WardWithPatient wardWithPatientatient){
        try{
            dao.updateEntity(wardWithPatientatient);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void delete(int id){
        try{
            dao.deleteEntity(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

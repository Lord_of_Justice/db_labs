package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.controller.Entities.PatientDiagnose;

import java.sql.SQLException;
import java.util.List;

public class PatientDiagnoseCrud {
    DAOImpl<PatientDiagnose> dao;

    public PatientDiagnoseCrud(){
        dao = new DAOImpl<>(PatientDiagnose.class);
    }

    public PatientDiagnose get(int id) throws SQLException {
        return dao.getEntity(id);
    }
    public List<PatientDiagnose> getAll() throws SQLException{
        return dao.getEntityList();
    }

    public void insert(PatientDiagnose patientDiagnose){
        try{
            dao.insertEntity(patientDiagnose);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void update(PatientDiagnose patientDiagnose){
        try{
            dao.updateEntity(patientDiagnose);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void delete(int id){
        try{
            dao.deleteEntity(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

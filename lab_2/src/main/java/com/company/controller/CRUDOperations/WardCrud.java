package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.controller.Entities.Ward;

import java.sql.SQLException;
import java.util.List;

public class WardCrud {
    DAOImpl<Ward> dao;

    public WardCrud(){
        dao = new DAOImpl<>(Ward.class);
    }

    public Ward get(int id) throws SQLException {
        return dao.getEntity(id);
    }
    public List<Ward> getAll() throws SQLException{
        return dao.getEntityList();
    }
    public void insert(Ward ward){
        try{
            dao.insertEntity(ward);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void update(Ward ward){
        try{
            dao.updateEntity(ward);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void delete(int id){
        try{
            dao.deleteEntity(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package com.company.controller.CRUDOperations;

import com.company.dao.DAOImpl;
import com.company.controller.Entities.Doctor;

import java.sql.SQLException;
import java.util.List;

public class DoctorCrud {
    DAOImpl<Doctor> dao;

    public DoctorCrud(){
        dao = new DAOImpl<>(Doctor.class);
    }

    public Doctor get(int id) throws SQLException {
        return dao.getEntity(id);
    }
    public List<Doctor> getAll() throws SQLException{
        return dao.getEntityList();
    }
    public void insert(Doctor doctor){
        try{
            dao.insertEntity(doctor);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void update(Doctor patient){
        try{
            dao.updateEntity(patient);
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void delete(int id){
        try{
            dao.deleteEntity(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

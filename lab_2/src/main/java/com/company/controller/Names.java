package com.company.controller;

public enum Names {
    Dmytro,
    Vlad,
    Ivan,
    Eugene,
    Max,
    Volodymyr,
    Artem,
    Mykhailo,
    Igor,
    Oleksii,
    Anna,
    Nadia,
    Sophia,
    Olga,
    Nastya,
    Karina,
    Masha,
    Alina,
    Sveta,
    Kseniya,
}

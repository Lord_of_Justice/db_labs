package com.company.view;

import com.company.controller.Controller;
import com.company.controller.Entities.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class View {
    Controller controller;
    Logs logs;
    Scanner scanner;

    public View(){
        controller = new Controller();
        logs = new Logs();
    }
    public void mainMenu(){
        scanner = new Scanner(System.in);
        while(true){
            logs.showMainMenu();
            int number = Integer.parseInt(scanner.nextLine());
            if (number > 0 && number < 9){
                tablesMenu(number);
            } else if (number == 9){
                menuSearchFrom3Table();
            } else if (number == 10){
                fullTextSearchMenu();
            } else if (number == 11){
                System.out.println("Write amount: ");
                int amount = Integer.parseInt(scanner.nextLine());
                controller.generateRandomDataInPatient(amount);
            } else if (number == 12){
                break;
            }
        }
        scanner.close();
    }
    private void tablesMenu(int number){
        int choice = 0;
        do{
            logs.showMenuTableOperations(TableNames.values()[number - 1].toString());
            try {
                choice = Integer.parseInt(scanner.nextLine());
                switch (choice) {
                    case 1:
                        showAllEntities(number);
                        break;
                    case 2:
                        FindEntityById(number);
                        break;
                    case 3:
                        insertEntityById(number);
                        break;
                    case 4:
                        updateEntityById(number);
                        break;
                    case 5:
                        deleteEntityById(number);
                        break;
                }
            } catch (Exception e){
                e.getMessage();
            }
        } while (choice != 6);
    }

    private <T> void showAllEntities(int number) throws SQLException {
        if (number == 1) {
            List<Diagnosis> diagnoses = controller.diagnose.getAll();
            printEntity(diagnoses);
        } else if (number == 2) {
            List<Doctor> doctors = controller.doctor.getAll();
            printEntity(doctors);
        } else if(number == 3) {
            List<ListOfPatients> list = controller.listOfPatients.getAll();
            printEntity(list);
        } else if(number == 4) {
            List<MedicalOpinion> list = controller.medicalOpinion.getAll();
            printEntity(list);
        } else if (number == 5) {
            List<Patient> list = controller.patient.getAll();
            printEntity(list);
        } else if (number == 6) {
            List<PatientDiagnose> list = controller.patientDiagnose.getAll();
            printEntity(list);
        } else if (number == 7) {
            List<Ward> list = controller.ward.getAll();
            printEntity(list);
        } else if (number == 8) {
            List<WardWithPatient> list = controller.wardWithPatient.getAll();
            printEntity(list);
        }
    }
    private void FindEntityById(int number) throws SQLException {
        Entity entity = null;
        System.out.println("Write index: ");
        int index = Integer.parseInt(scanner.nextLine());
        if (number == 1) {
            entity = controller.diagnose.get(index);
        } else if (number == 2) {
            entity = controller.doctor.get(index);
        } else if(number == 3) {
            entity = controller.listOfPatients.get(index);
        } else if(number == 4) {
            entity = controller.medicalOpinion.get(index);
        } else if (number == 5) {
            entity = controller.patient.get(index);
        } else if (number == 6) {
            entity = controller.patientDiagnose.get(index);
        } else if (number == 7) {
            entity = controller.ward.get(index);
        } else if (number == 8) {
            entity = controller.wardWithPatient.get(index);
        }
        entity.print();
    }
    private void insertEntityById(int number) {
        System.out.println("Write index: ");
        if (number == 1) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write name: ");
            String name = scanner.nextLine();
            System.out.println("Write symptoms: ");
            String symptoms = scanner.nextLine();
            System.out.println("Write therapy: ");
            String therapy = scanner.nextLine();
            controller.diagnose.insert(new Diagnosis(index, name, symptoms, therapy));
        } else if (number == 2) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write name: ");
            String name = scanner.nextLine();
            System.out.println("Write specilty: ");
            String specilty = scanner.nextLine();
            controller.doctor.insert(new Doctor(index, name, specilty));
        } else if(number == 3) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write patient index: ");
            int ptid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write doctor index: ");
            int docid = Integer.parseInt(scanner.nextLine());
            controller.listOfPatients.insert(new ListOfPatients(index, ptid, docid));
        } else if(number == 4) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write doctor index: ");
            int docid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write diagnose index: ");
            int dgid = Integer.parseInt(scanner.nextLine());
            controller.medicalOpinion.insert(new MedicalOpinion(index, docid, dgid));
        } else if (number == 5) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write name: ");
            String name = scanner.nextLine();
            System.out.println("Write age: ");
            int age = Integer.parseInt(scanner.nextLine());
            System.out.println("Write illness time: ");
            int illnessTime = Integer.parseInt(scanner.nextLine());
            System.out.println("Write male: ");
            boolean male = Boolean.parseBoolean(scanner.nextLine());
            controller.patient.insert(new Patient(index, name, age, illnessTime, male));
        } else if (number == 6) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write patient index: ");
            int ptid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write diagnose index: ");
            int dgid = Integer.parseInt(scanner.nextLine());
            controller.patientDiagnose.insert(new PatientDiagnose(index, ptid, dgid));
        } else if (number == 7) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write patient index: ");
            int wdnumber = Integer.parseInt(scanner.nextLine());
            System.out.println("Write name: ");
            String unit = scanner.nextLine();
            System.out.println("Write patient index: ");
            int peopleAmount = Integer.parseInt(scanner.nextLine());
            controller.ward.insert(new Ward(index, wdnumber, unit, peopleAmount));
        } else if (number == 8) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write patient index: ");
            int ptid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write ward index: ");
            int wrdid = Integer.parseInt(scanner.nextLine());
            controller.wardWithPatient.insert(new WardWithPatient(index, ptid, wrdid));
        }
    }
    private void updateEntityById(int number) {
        System.out.println("Write index: ");
        if (number == 1) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write name: ");
            String name = scanner.nextLine();
            System.out.println("Write symptoms: ");
            String symptoms = scanner.nextLine();
            System.out.println("Write therapy: ");
            String therapy = scanner.nextLine();
            controller.diagnose.update(new Diagnosis(index, name, symptoms, therapy));
        } else if (number == 2) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write name: ");
            String name = scanner.nextLine();
            System.out.println("Write specilty: ");
            String specilty = scanner.nextLine();
            controller.doctor.update(new Doctor(index, name, specilty));
        } else if(number == 3) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write patient index: ");
            int ptid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write doctor index: ");
            int docid = Integer.parseInt(scanner.nextLine());
            controller.listOfPatients.update(new ListOfPatients(index, ptid, docid));
        } else if(number == 4) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write doctor index: ");
            int docid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write diagnose index: ");
            int dgid = Integer.parseInt(scanner.nextLine());
            controller.medicalOpinion.update(new MedicalOpinion(index, docid, dgid));
        } else if (number == 5) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write name: ");
            String name = scanner.nextLine();
            System.out.println("Write age: ");
            int age = Integer.parseInt(scanner.nextLine());
            System.out.println("Write illness time: ");
            int illnessTime = Integer.parseInt(scanner.nextLine());
            System.out.println("Write male: ");
            boolean male = Boolean.parseBoolean(scanner.nextLine());
            controller.patient.update(new Patient(index, name, age, illnessTime, male));
        } else if (number == 6) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write patient index: ");
            int ptid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write diagnose index: ");
            int dgid = Integer.parseInt(scanner.nextLine());
            controller.patientDiagnose.update(new PatientDiagnose(index, ptid, dgid));
        } else if (number == 7) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write patient index: ");
            int wdnumber = Integer.parseInt(scanner.nextLine());
            System.out.println("Write name: ");
            String unit = scanner.nextLine();
            System.out.println("Write patient index: ");
            int peopleAmount = Integer.parseInt(scanner.nextLine());
            controller.ward.update(new Ward(index, wdnumber, unit, peopleAmount));
        } else if (number == 8) {
            int index = Integer.parseInt(scanner.nextLine());
            System.out.println("Write patient index: ");
            int ptid = Integer.parseInt(scanner.nextLine());
            System.out.println("Write ward index: ");
            int wrdid = Integer.parseInt(scanner.nextLine());
            controller.wardWithPatient.update(new WardWithPatient(index, ptid, wrdid));
        }
    }
    private void deleteEntityById(int number){
        System.out.println("Write index: ");
        int index = Integer.parseInt(scanner.nextLine());
        if (number == 1) {
            controller.diagnose.delete(index);
        } else if (number == 2) {
            controller.doctor.delete(index);
        } else if(number == 3) {
            controller.listOfPatients.delete(index);
        } else if(number == 4) {
            controller.medicalOpinion.delete(index);
        } else if (number == 5) {
            controller.patient.delete(index);
        } else if (number == 6) {
            controller.patientDiagnose.delete(index);
        } else if (number == 7) {
            controller.ward.delete(index);
        } else if (number == 8) {
            controller.wardWithPatient.delete(index);
        }
    }

    private <T> void printEntity(List<T> list){
        for (T t: list) {
            Entity entity = (Entity) t;
            entity.print();
        }
    }

    private void menuSearchFrom3Table(){
        int choice = -1;
        do {
            logs.showFrom3TableMenu();
            try {
                choice = Integer.parseInt(scanner.nextLine());
                switch (choice){
                    case 1: showSearchFrom3Table(controller.StaticSearch1()); break;
                    case 2: showSearchFrom3Table(controller.StaticSearch2()); break;
                    case 3:
                        System.out.println("Write left bound: ");
                        int left = Integer.parseInt(scanner.nextLine());
                        System.out.println("Write right bound: ");
                        int right = Integer.parseInt(scanner.nextLine());
                        System.out.println("Write male(boolean): ");
                        boolean male = Boolean.parseBoolean(scanner.nextLine());
                        showSearchFrom3Table(controller.DynamicSearch(left, right, male)); break;
                    case 4: break;
                    default:System.out.println("Incorrect input");
                }
            } catch (NumberFormatException numberError) {
                System.out.println("Incorrect input!");
            }
        } while (choice != 4);
    }
    private void fullTextSearchMenu() {
        int choise = -1;
        do {
            logs.showFullTextSearchMenu();
            try {
                choise = Integer.parseInt(scanner.nextLine());
                if(choise == 1)
                    wordSearchMenu();
                else if (choise == 2)
                    phraseSearchMenu();
                else{
                    if(choise !=3)
                        System.out.println("Incorrect input");
                }
            } catch (NumberFormatException numberError) {
                System.out.println("Incorrect input!");}
        } while (choise != 3);
    }
    private void wordSearchMenu () {
        System.out.println("Enter word to search");
        String wordToSearch = scanner.nextLine();
        ResultSet rs = controller.diagnose.fullTextSearch(wordToSearch, false);
        System.out.println("Word Search Result");
        try {
            if (!rs.isBeforeFirst()){
                System.out.println("Nothing was found!");
            } else {
                showTextSearchResult(rs);
            }
        }catch (SQLException sqlExcept){
            sqlExcept.printStackTrace();
        }
    }
    private void phraseSearchMenu () {
        System.out.println("Enter phrase to search");
        String phraseToSearch = scanner.nextLine();
        ResultSet rs = controller.diagnose.fullTextSearch(phraseToSearch, true);
        System.out.println("Phrase Search Result");
        try {
            if (!rs.isBeforeFirst()){
                System.out.println("Nothing was found!");
            } else {
                showTextSearchResult(rs);
            }
        }catch (SQLException sqlExcept){
            sqlExcept.printStackTrace();
        }
    }
    private void showTextSearchResult(ResultSet rs){
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("dg_name: " + rs.getString(1));
                System.out.println("dg_symptoms: " + rs.getString(2));
            }
            System.out.println("-----------------------------------");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void showSearchFrom3Table(ResultSet rs){
        try{
            while(rs.next()){
                System.out.println("-----------------------------------");
                System.out.println("pt_id: " + rs.getInt(1));
                System.out.println("pt_name: " + rs.getString(2));
                System.out.println("pt_age: " + rs.getInt(3));
                System.out.println("pt_illnessTime: " + rs.getInt(4));
                System.out.println("pt_male: " + rs.getBoolean(5));
                System.out.println("dg_name: " + rs.getString(6));
                System.out.println("dg_symptoms: " + rs.getString(7));
                System.out.println("dg_therapy: " + rs.getString(8));
            }}catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }
}

package com.company.view;

public class Logs {
    public void showMainMenu() {
        System.out.println("========== Main Menu ==========");
        System.out.println("Choose table or operation: ");
        for (int i = 0; i < TableNames.values().length; i++){
            System.out.println(
                    String.format("%d) Table %s", i + 1, TableNames.values()[i].toString()));
        }
        System.out.println("9) Search from 3 table");
        System.out.println("10) Full Text Search");
        System.out.println("11) Random gen");
        System.out.println("12) Exit\n-> ");
    }
    public void showMenuTableOperations(String tableName){
        System.out.println("========== Table " + tableName + " Menu ==========" );
        System.out.println("1) ShowAll\n2) Find by index");
        System.out.println("3) Insert\n4) Update");
        System.out.println("5) Delete\n6) To Main Menu\n-> ");
    }
    public void showFrom3TableMenu(){
        System.out.println("========== Search From 3 Table ==========");
        System.out.println("1) Static search 1");
        System.out.println("2) Static search 2");
        System.out.println("3) Dynamic search");
        System.out.println("4) To main menu");
        System.out.print("-> ");
    }
    public void showFullTextSearchMenu() {
        System.out.println("1) Word search from review texts");
        System.out.println("2) Phrase search from review texts");
        System.out.println("3) To main menu");
        System.out.print("-> ");
    }
}
